# Car Dealer Fullstack Project

<p>Project for the Data Structures and Algorithm course in UPR- Mayaguez for Fall 2018</p>

## Car Manager URI
    -- cardealer/cars/ (get all cars)
    -- cardealer/cars/year/{carYear} (get cars by year)
    -- cardealer/cars/add (adds a new car assuming unique ID)
    -- cardealer/cars/id (get car by ID)
    -- cardealer/cars/id/update (update car by ID)
    -- cardealer/cars/id/delete (delete car by ID)
    
## CarUnitManager URI
    --cardealer/carunit/ (get all carunits)
    --cardealer/carunit/add (adds a new carunit assuming unique ID)
    --cardealer/carunit/VIN/{VIN} (get car by VIN)
    --cardealer/cars/{carUnitId} (get carunit by ID)
    --cardealer/cars/{carUnitId}/update (update carUnit by ID)
    --cardealer/cars/{carUnitId}/delete (delete carUnit by ID)

##  PersonManager URI
    --cardealer/person/ (get all person objects)
    --cardealer/person/add (adds a new person assuming unique ID)
    --cardealer/person/lastname/{lastNamee} (get person by lastname)
    --cardealer/person/{Id} (get person by ID)
    --cardealer/person/{Id}/update (update person by ID)
    --cardealer/person/{Id}/delete (delete person by ID)
    
## AppointmentManager URI
    --cardealer/appointment (gets all appointments)
    --cardealer/appointment/day/{day} [default works with int but String works, must uncomment code](filters by day)
    --cardealer/appointment/{id} (gets appointment by id[long])
    --cardealer/appointment/{id}/update (updates an existing Appointment with specified ID)
    --cardealer/appointment/{id}/delete (deletes an existing Appointment with specified ID)