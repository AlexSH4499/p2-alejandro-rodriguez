package edu.uprm.cse.datastructures.cardealer.util;


import java.util.Comparator;
import java.util.Iterator;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

import interfaces.Node;
import interfaces.SortedList;

/**
 * A class of Circular List that is sorted based on the provided Comparator for @param <E>.
 * Internally the list uses Doubly Linked Nodes with a dummy header.
 * 
 * @author Alejandro Rodriguez Natal
 *
 * @param <E> Object which the list will use to be sorted by.
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E>
{
	private DNode<E> first;
	private int size;
	private AbstractComparator<E> comp;
	
	//Uses dummy header
	public CircularSortedDoublyLinkedList()
	{
		this.comp = (AbstractComparator<E>) comparator();
		this.first = new DNode<E>(null,this.first,this.first);
		this.size = 0;
	}
	
	public CircularSortedDoublyLinkedList(AbstractComparator<E> comp)
	{
		this.comp = comp;
		this.first = new DNode<E>(null,this.first,this.first);
		this.size = 0;		
	}
	
	/**
	 * Creates a new CircularIterator of this list instance with Header  Node as an internal parameter.
	 */
	public Iterator<E> iterator() 
	{
		return  (Iterator<E>) new CircularIterator(this.first);
	}

	/**
	 * Creates an AbstractComparator in case one was not previously provided.
	 * @return Comparator<E> object.
	 */
	public Comparator<E> comparator()
	{
		return new AbstractComparator<E>();
	}
	
	/**
	 * Adds a new object @param obj at the end of current instance of list.
	 * 
	 * @return boolean representing if the transaction was successful.
	 */
	public boolean add(E obj)
	{
		if(contains(obj))return true;	
		if(this.isEmpty())
		{
			DNode<E> temp = new DNode(obj,this.first, this.first);
			this.first.setNext(temp);
			this.first.setPrev(temp);
			this.size++;
			return false;
		}
		else{
			DNode<E>current = this.first.getPrev();
			DNode<E> nuevo = new DNode(obj, current, this.first);
			current.setNext(nuevo);
			this.first.setPrev(nuevo);
		}
		
		this.size++;
		//Triple sort because we're too dumb to find an efficient execution
		arraySort();
		arraySort();
		arraySort();
		return false;
	}

	/**
	 * Retrieves the current size of list instance.
	 * 
	 *@return int representing size of list.
	 */
	public int size()
	{
		return this.size;
	}

	/**
	 * Removes an object @param obj from current instance of list if it exists.
	 * 
	 * @return boolean representing if the transaction was successful.
	 */
	public boolean remove(E obj)
	{
		if(isEmpty() | !contains(obj))
			return false;
		
		boolean removed = false;
		
		DNode<E> ptr = this.first.getNext();
		for(int i = 0 ; i < this.size(); i++)
		{
			if(ptr.getElement().equals(obj))
			{
				ptr.getPrev().setNext(ptr.getNext());
				ptr.getNext().setPrev(ptr.getPrev());
				
				ptr.setElement(null);
				this.size--;
				removed = true;
				break;
			}
			ptr = ptr.getNext();
		}
		
		return removed;
	}

	/**
	 * Retrieves the first Node of this list.
	 * @return Node representing first Node of this list. 
	 */
	protected Node<E> getFirstNode()
	{
		return this.first.getNext();
	}
	
	/**
	 * Removes object at @param index from current instance of list if it exists and is in range of list.
	 * 
	 * @return boolean representing if the transaction was successful.
	 */
	public boolean remove(int index)
	{
		if(index < 0 | index >= this.size())
			return false;
		
		DNode<E> ptr = this.first.getNext();
		int i = 0;
		while(i <= index)
		{
			ptr = ptr.getNext();
			i++;
		}
		ptr.getPrev().setNext(ptr.getNext());
		ptr.getNext().setPrev(ptr.getPrev());
		
		ptr.setElement(null);

		
		this.size--;
		return true;
	}

	/**
	 * Removes all copies of object @param obj from current instance of list if any exists.
	 * 
	 * @return boolean representing if the transaction was successful.
	 */
	public int removeAll(E obj) 
	{
		if(!this.contains(obj))
			return 0;
		
		int i = 0;
		while(this.contains(obj))
		{
			remove(obj);
			i++;
		}
		return i;
	}

	/**
	 * Retrieves the first non dummy element within the instance of list.
	 * 
	 * @return E element at first Position.
	 */
	public E first() {
		return this.first.getNext().getElement();
	}

	//Can be more efficient but whatever
	/**
	 * Retrieves the first non dummy element within the instance of list.
	 * 
	 * @return E element at last Position.
	 */
	public E last() {
		DNode<E> ptr = this.first;
		
		while(!ptr.getNext().equals(this.first))
		{
			ptr = ptr.getNext();
		}
		return ptr.getElement();
	}

	/**
	 * Retrieves the n-th  non dummy element within the instance of list at @param index.
	 * 
	 * @return E element at @param index Position.
	 */
	public E get(int index)
	{
		if(index < 0 |index > this.size())
			return null;
		
		DNode<E> ptr = this.first;
		int i = 0;
		while(i <= index)
		{
			ptr = ptr.getNext();
			i++;
		}
		return ptr.getElement();
	}

	/**
	 * Empties the current instance of the list.
	 */
	public void clear()
	{
		if(this.isEmpty())
			return;
		
		DNode<E> temp = this.first;
		DNode<E> after = temp.getNext();
		while(!after.getNext().equals(null))
		{
			temp.cleanLinks();
			temp.setElement(null);
			temp = after;
			after = after.getNext();
			this.size--;
		}
	}

	/**
	 * Verifies existence of @param e in current instance of list if any.
	 * 
	 * @return boolean representing if the transaction was successful.
	 */
	public boolean contains(E e) 
	{
		if(isEmpty())return false;
		
		DNode<E> ptr = this.first.getNext();
		boolean contains = false;

		for(int i = 0; i < this.size()  ; i++, ptr = ptr.getNext())
		{
			if(ptr.getElement().equals(e))
			{
				contains = true;
				break;
			}

		}
		ptr = null;
		return contains;
	}

	
	/**
	 * Verifies size of current instance of list.
	 * 
	 * @return boolean representing if the list is empty or not.
	 */
	public boolean isEmpty() {
		return this.size() == 0;
	}

	//TODO Might be wrong, change to a for loop
	/**
	 * Finds first index where @param e is located at within the list.
	 * 
	 * @return int representing index of @param e, or -1 if not in list.
	 */
	public int firstIndex(E e)
	{
		//Returns -1 if list is empty
		if(this.isEmpty())
			return -1;
		Iterator<E> iter = iterator();
		E temp;
		int ind = 0;
		while(iter.hasNext())
		{
			temp = iter.next();
			ind++;
			if(temp.equals(e));
				return ind;
			
			
		}
		
		return -1;
	}

	/**
	 * Finds last index where @param e is located at within the list.
	 * 
	 * @return int representing index of @param e, or -1 if not in list.
	 */
	public int lastIndex(E e) 
	{
		
		if(this.isEmpty() || !this.contains(e))
			return -1;
		
		Node<E> temp = this.first.getNext();
		int ind = 0;
		
		for(int i = 0; i < this.size(); i++, temp = temp.getNext())
		{
			if(temp.getElement().equals(e))
				ind = i;
		}
		
		return ind;
	}
	
	/**
	 * Adds @param nuevo to the  beginning of this list's instance.
	 *  
	 * @param nuevo Node which we want to add to  the front of the list.
	 */
	private void addFirstNode(DNode<E> nuevo)
	{
		addNodeAfter(this.first, nuevo); 
	}

	/**
	 * Converts this list instance to array and sorts as an array.
	 */
	private void arraySort()
	{
		E[] arr = this.toArray();
		
		for(int i = 0; i < arr.length-1; i++)
		{
			if(comp.compare(arr[i], arr[i+1])>0)
				swap(arr,i, i+1);

		}
		
		for(int i = 0; i < arr.length-1; i++)
		{
			if(comp.compare(arr[i], arr[i+1])>0)
				swap(arr,i, i+1);
		}
		
		for(int i = 0; i < arr.length-1; i++)
		{
			if(comp.compare(arr[i], arr[i+1])>0)
				swap(arr,i, i+1);
		}
		
		DNode<E> current = this.first.getNext();
		//DNode<E> current = this.first;
		for(int i = 0 ; i <arr.length; i++)
		{
			current.setElement(arr[i]);
			current =current.getNext();
		}
	}
	
	/**
	 * Private sorting method that guarantees our objects are kept in order.
	 */
	private void sort()
	{
		int n = 0;
		DNode<E> temp = this.first;
		DNode<E> after = this.first.getNext();
		while(!after.getNext().equals(this.first))
		//while(iter.hasNext())
		{
			if(temp.getElement()!=null | after.getElement()!=null )
				n = comp.compare(temp.getElement(),after.getElement());
			if(temp.getElement()== null |after.getElement()==null )
				n=0;
			switch(n)
			{
				case 1:
					swapElements(temp, after);
					break;
			
					
				default:
					break;
			}
			temp = after;
			after = after.getNext();
		}
		
		temp = after;
		after = after.getNext();
		 n = comp.compare(temp.getElement(),after.getElement());
		switch(n)
		{
			case 1:
				swapElements(temp ,after);
				break;
		
				
			default:
				break;
		}
	}
	
	/**
	 * Private method for swapping elements in an array.
	 * @param arr array in which we want to swap two elements
	 * @param target index which we want to swap
	 * @param rec index of destination to swap
	 */
	private void swap(E[] arr,int target, int rec)
	{
		E temp = arr[rec];
		arr[rec] = arr[target];
		arr[target] = temp;
	}
	
	/**
	 * Private method for swapping node elements in an a linked list.
	 * @param target Node which we want to swap
	 * @param rec Node of destination to swap
	 */
	private void swapElements(DNode<E> target, DNode<E> rec)
	{
		E temp = target.getElement();
		target.setElement(rec.getElement());
		rec.setElement(temp);
	}
	
	/**
	 * Creates an array of the current instance of list.
	 * @return Array representation of current list.
	 */
	public E[] toArray()
	{
		DNode<E> ptr = this.first.getNext();
		E[] arr = (E[])new Object[this.size()];
		
		for(int i = 0 ; i < this.size(); i++)
		{
			arr[i] = ptr.getElement();
			ptr = ptr.getNext();
		}
		
		return arr;
	}
	
	/**
	 * Prints the current list to Standard Output.
	 */
	public void printList()
	{
		DNode<E> ptr = this.first;
		while(ptr.getNext()!= this.first)
		{
			System.out.println(ptr);
			ptr = ptr.getNext();
		}
		System.out.println(ptr);
	}
	
	/**
	 * Adds @param nuevo after @param target in this list's instance.
	 * @param target Node after which we want to add the new one. 
	 * @param nuevo Node which we want to add to list.
	 */
	private void addNodeAfter(DNode<E> target, DNode<E> nuevo) {
		
		if(target.equals(this.getLastNode()))
		{
			addLastNode(nuevo);
			return;
		}
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> nBefore = (DNode<E>) target; 
		DNode<E> nAfter = nBefore.getNext(); 
		nBefore.setNext(dnuevo); 
		nAfter.setPrev(dnuevo); 
		dnuevo.setPrev(nBefore); 
		dnuevo.setNext(nAfter); 
		
		this.size++; 
	}
	
	/**
	 * Adds @param nuevo to the end of  this list instance. 
	 * @param nuevo Node representing the last position in the list.
	 */
	private void addLastNode(DNode<E> nuevo)
	{ 
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> trailer = this.getLastNode();
		DNode<E> nBefore = trailer.getPrev();  
		
		nBefore.setNext(dnuevo); 
		trailer.setPrev(dnuevo); 
		dnuevo.setPrev(nBefore); 
		dnuevo.setNext(trailer); 
		
		this.size++; 
	}

	
	/**
	 * Retrieves this list instance's last node. 
	 * @return Node representing the last position in the list.
	 */
	private DNode<E> getLastNode()
	{
		if(isEmpty())
			return this.first;
		DNode<E> ptr = this.first;
		while(!ptr.getNext().equals(this.first))
			ptr = ptr.getNext();
		
		return ptr;
	}


	/**
	 * Adds @param nuevo before @param target in this list's instance.
	 * @param target Node before which we want to add the new one. 
	 * @param nuevo Node which we want to add to list.
	 */
	private void addNodeBefore(DNode<E> target, DNode<E> nuevo)
	{
		if(target.equals(this.first))
		{
			addFirstNode(nuevo);
		}
		
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> nBefore = ((DNode<E>) target).getPrev(); 
		
		nBefore.setNext(dnuevo); 
		dnuevo.setPrev(nBefore); 
		dnuevo.setNext((DNode<E>)target);
		((DNode<E>)target).setPrev(dnuevo);
		 
		this.size++;
	}


	private static class DNode<E>  implements Node<E>
	{
		private E element; 
		private DNode<E> prev, next; 

		// Constructors
		public DNode() {this(null);}
		
		public DNode(E e) { 
			this( e,null,null); 
			
		}
		
		public DNode(E e, DNode<E> p, DNode<E> n) { 
			prev = p; 
			next = n;
			element = e;
		}
		
		
		public String toString()
		{
			return "{"+this.getElement()+ "} ";
			
		}
		// Methods
		public DNode<E> getPrev() {
			return prev;
		}
		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}
		public DNode<E> getNext() {
			return next;
		}
		public void setNext(DNode<E> next) {
			this.next = next;
		}
		public E getElement() {
			return element; 
		}

		public void setElement(E data) {
			element = data; 
		} 
		
		/**
		 * Just set references prev and next to null. Disconnect the node
		 * from the linked list.... 
		 */
		public void cleanLinks() { 
			prev = next = null; 
		}
		
	
	}

	public class CircularIterator<T> implements Iterator<Node<T>> {

		 private final Node<T> first;
		    private Node<T> mPosition;


		    public CircularIterator(Node<T> first) {
		        this.first = first;
		        this.mPosition = this.first;
		        
		    }

		   
		    public boolean hasNext() {
		        return !this.mPosition.getNext().equals(this.first);
		    }

		    
		    public Node<T> next() {
		        if (!hasNext())
		            return this.first;

		        mPosition = mPosition.getNext();

		        return  mPosition;
		    }
	
	}


	
}

	
