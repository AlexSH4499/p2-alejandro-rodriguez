package edu.uprm.cse.datastructures.cardealer;

import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.model.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.MockPersonList;
import exceptions.JsonError;
import exceptions.NotFoundException;

/**
 * A Manager for Person objects
 * Keeps an ArrayList of type Person
 * Implements a policy to dictate each person has a unique id.
 * @author Alejandro Rodriguez Natal
 *
 */
@Path("person")
public class PersonManager {
	private static  CopyOnWriteArrayList<Person> cList = MockPersonList.getInstance();   
	private CircularSortedDoublyLinkedList<Person> sorting;
	
	/**
	 * Keeps our inner list sorted
	 * @param cList the list we wish to sort.
	 */
	private void updateList(CopyOnWriteArrayList<Person> cList)
	{
		sorting = new  CircularSortedDoublyLinkedList<Person>(new PersonComparator());

		for(Person a: cList) {
			sorting.add(a);
		}
		cList.clear();
		
		for(int i=0;i<sorting.size();i++) 
		{
			if(!cList.contains((Person) sorting.get(i)))
				cList.add((Person) sorting.get(i));
			
		}
	
	}
	
	/**
	 * Returns an array of type Person representing the internal list. 
	 * @return array of type Person.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getAllPeople()
	{
		updateList(cList);
		return cList.toArray(new Person[0]);
	}
	
	/**
	 *  Retrieves the Person object with id matching @param id
	 * @param id The id of the Person object we wish to retrieve
	 * @return The Person object with provided id.
	 */
	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Person getPerson(@PathParam("id") long id){
	    Optional<Person> match
	        = cList.stream()
	        .filter(c->c.getID() == id)
	        .findFirst();
	    if (match.isPresent()) {
	    	updateList(cList);
	      return match.get();
	    } else {
	      throw new NotFoundException(new JsonError("Error", "Person " + id + " not found"));
	    }
	 }     
	  
		/**
		 *  Retrieves the Person object with last name matching @param ln
		 * @param ln The last name  of the Person object we wish to retrieve
		 * @return The Person object with provided last name.
		 */
	  @GET
	  @Path("lastname/{lastName}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Person[] getPersonByLN(@PathParam("lastName") String ln)
	  {
		  if(cList.isEmpty()) throw new NotFoundException(new JsonError("Error", "People of lastname " + ln + " not found"));
		  CopyOnWriteArrayList<Person> dummyList = new CopyOnWriteArrayList(); 
		  Iterator<Person> iter = cList.iterator();
		  Person ptr = iter.next();
		  if(ptr.getLastName().equalsIgnoreCase( ln))
		  {
			  System.out.println(ptr);
			  dummyList.add(ptr);
		  }
		  while(iter.hasNext())
		  {
			  if(ptr.getLastName().equalsIgnoreCase( ln))
			  {
				  System.out.println(ptr);
				  dummyList.add(ptr);
			  }
			  ptr = iter.next();
		  }
	  
	    if (!dummyList.isEmpty())
	    {
	    	updateList(dummyList);
	      return dummyList.toArray(new Person[0]);
	    } else {
	      throw new NotFoundException(new JsonError("Error", "People of lastname " + ln + " not found"));
	    }
	 }     
	    
	  /**
	   * Adds @param p to our inner list
	   * @param p Person we wish to add to list
	   * @return HTTP Response representing state of the transaction.
	   */
	   @POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)//TODO add id checker so we dont people with the same id
	    public Response addPerson(Person p)
	   {
		   boolean isInList = false;
		   for(Person pe: cList)
		   {
			   if(p.getID() == pe.getID())
			   {
				   isInList = true;break;
			   }
		   }
		   
		   if(!isInList)
			   cList.add(p);
	      updateList(cList);
	      return Response.status(201).build();
	    }  
	   
		/**
		 *  Updates the Person object with id matching Path id
		 * @param p The Person object we wish to update
		 * @return  HTTP Response representing state of the transaction..
		 */
	   @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updatePerson(Person p){
	      int matchIdx = 0;
	      Optional<Person> match = cList.stream()
	          .filter(c->c.getID() == p.getID())
	          .findFirst();
	      if (match.isPresent()) {
	        matchIdx = cList.indexOf(match.get());
	        cList.set(matchIdx, p);
	        updateList(cList);
	        return Response.status(Response.Status.OK).build();
	      } else {
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      }
	    }
	   
	   /**
		 *  Deletes the Person object with @param id 
		 * @param id The id of Person object we wish to delete
		 * @return  HTTP Response representing state of the transaction.
		 */
	    @DELETE
	    @Path("/{id}/delete")
	    public Response deletePerson(@PathParam("id") long id){
	      Predicate<Person> p = (c->c.getID() == id);
	      if (!cList.removeIf(p)) {
	       throw new NotFoundException(new JsonError("Error", "Person " + id + " not found"));
	      }
	      else {
	    	  Response temp = Response.status(Response.Status.OK).build();
	    	  return temp ;
	      }
	    } 
}
