package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.AbstractComparator;
/**
 * CarUnit comparator used to order CarUnits in a list.
 * The criteria for CarUnit sorting is based on VIN.
 * @author Alejandro Rodriguez Natal
 *
 */
public class CarUnitComparator extends AbstractComparator<CarUnit> {
	
	@Override
	public int compare(CarUnit u1 , CarUnit u2)
	{
		return u1.getVIN().compareToIgnoreCase(u2.getVIN());
	}

}
