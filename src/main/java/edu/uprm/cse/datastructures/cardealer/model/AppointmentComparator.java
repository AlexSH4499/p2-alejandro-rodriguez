package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.AbstractComparator;

/**
 * Appointment comparator used to order Appointments in a list.
 * The criteria for Appointment sorting is based on their unique ID's.
 * @author Alejandro Rodriguez Natal
 *
 */
public class AppointmentComparator extends AbstractComparator<Appointment>
{

	@Override
	public int compare(Appointment a1, Appointment a2) 
	{
		if(a1.getAppointmentId() < a2.getAppointmentId())
			return 1;
		
		if(a1.getAppointmentId() > a2.getAppointmentId())
			return -1;
		
		return 0;
	}
}
