
package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

import interfaces.Node;

/**
 * Circular iterator used to traverse a circular list. 
 * That is to say, a list whose last position points back to the first position. 
 * 
 * @author Alejandro Rodriguez Natal
 *
 */
public class CircularIterator<T> implements Iterator<Node<T>>
{
	 private final Node<T> first;
	    private Node<T> mPosition;


	    public CircularIterator(Node<T> first) {
	        this.first = first;
	        this.mPosition = this.first;
	        
	    }

	   /**
	    * Returns the state of the next Position existing in the list.
	    * Since it's circular, we verify that the next Position is not the first one again.
	    * 
	    * @return boolean representing if there is a next Position
	    */
	    public boolean hasNext() {
	        return !this.mPosition.getNext().equals(this.first);
	    }

		   /**
		    * Returns the Node<T> representation of the next Position existing in the list.
		    * Will only return the next Position as long as there is still more Positions to traverse in the list.
		    * 
		    * @return Node<T> representing next Position
		    */
	    public Node<T> next() {
	        if (!hasNext())
	            return this.first;

	        mPosition = mPosition.getNext();

	        return  mPosition;
	    }
	    
	    /**
	     * 
	     * @return an instance of this class.
	     */
	public Iterator<T> iterator() {
		return (Iterator<T>) new CircularIterator(this.first);
	}

}
