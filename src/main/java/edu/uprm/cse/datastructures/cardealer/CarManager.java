package edu.uprm.cse.datastructures.cardealer;

import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import exceptions.JsonError;
import exceptions.NotFoundException;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.MockCarList;

/**
 * Manages all Car objects and orders them by Brand, Model, Option and Year.
 * 
 * @author Alejandro Rodriguez Natal
 *
 */
@Path("cars")
public class CarManager
{
	
	private static  CopyOnWriteArrayList<Car> cList = MockCarList.getInstance();   
	private CircularSortedDoublyLinkedList<Car> sorting;
	

	public static CopyOnWriteArrayList<Car> getList()
	{
		return cList;
	}
	
	/**
	 * Keeps our inner list sorted
	 * @param cList the list we wish to sort.
	 */
	private void updateList(CopyOnWriteArrayList<Car> cList)
	{
		sorting = new  CircularSortedDoublyLinkedList<Car>(new CarComparator());

		for(Car a: cList) {
			sorting.add(a);
		}
		cList.clear();
		
		for(int i=0;i<sorting.size();i++) {
			cList.add((Car) sorting.get(i));
			
		}
	
	}
	
	/**
	 * Returns an array of type Car representing the internal list. 
	 * @return array of type Car.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars()
	{
		updateList(cList);
		return cList.toArray(new Car[0]);
	}
	
	/**
	 *  Retrieves the Car object with id matching @param id
	 * @param id The id  of the Car object we wish to retrieve
	 * @return The Car object with provided id.
	 */
	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car getCarById(@PathParam("carId") long id){
	    Optional<Car> match
	        = cList.stream()
	        .filter(c->c.getCarId() == id)
	        .findFirst();
	    if (match.isPresent()) {
	    	updateList(cList);
	      return match.get();
	    } else {
	      throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	    }
	 }     
	  
		/**
		 *  Retrieves the Car object with brand matching @param brand
		 * @param brand The brand  of the Car object we wish to retrieve
		 * @return The Car object with provided brand.
		 */
	  @GET
	  @Path("brands/{carBrand}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car[] getCarByBrand(@PathParam("carBrand") String brand)
	  {
		  CopyOnWriteArrayList<Car> dummyList = new CopyOnWriteArrayList(); 
		  Iterator<Car> iter = cList.iterator();
		  Car ptr = iter.next();
		  if(ptr.getCarBrand().equalsIgnoreCase( brand))
		  {
			  System.out.println(ptr);
			  dummyList.add(ptr);
		  }
		  while(iter.hasNext())
		  {
			  if(ptr.getCarBrand().equalsIgnoreCase( brand))
			  {
				  System.out.println(ptr);
				  dummyList.add(ptr);
			  }
			  ptr = iter.next();
		  }
	  
	    if (!dummyList.isEmpty())
	    {
	    	updateList(dummyList);
	      return dummyList.toArray(new Car[0]);
	    } else {
	      throw new NotFoundException(new JsonError("Error", "Cars of brand " + brand + " not found"));
	    }
	 }     
	  

		/**
		 *  Retrieves the Car object with year matching @param year
		 * @param year The year  of the Car object we wish to retrieve
		 * @return The Car object with provided year.
		 */
	  @GET
	  @Path("year/{carYear}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car[] getCarsByYear(@PathParam("carYear") int year)
	  {
		  CopyOnWriteArrayList<Car> dummyList = new CopyOnWriteArrayList(); 
		  for(Car c : cList)
		  {
			  if(c.getCarYear()== year)
			  {
				  System.out.println(c);
				  dummyList.add(c);
			  }
		  }
	    if (!dummyList.isEmpty()) {
	    	updateList(dummyList);
	      return dummyList.toArray(new Car[0]);
	    } else {
	      throw new NotFoundException(new JsonError("Error", "Cars of year " + year + " not found"));
	    }
	 }     
	  
	  /**
	   * Adds @param car to our inner list
	   * @param car Car we wish to add to list
	   * @return HTTP Response representing state of the transaction.
	   */
	   @POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addCar(Car car){
		   
		   boolean isInList = false;
		   for(Car c: cList)
		   {
			   if(c.getCarId() == car.getCarId())
			   {
				   isInList = true;break;
			   }
		   }
		   
		   if(!isInList)
		   {
			   cList.add(car);
			   updateList(cList);
			   return Response.status(201).build();
		   }
	      
	      return Response.status(201).build();//TODO PUT ERROR HERE because duplicate ID
	    }  
	   
	   
		/**
		 *  Updates the Car object with id matching Path id
		 * @param car The Car object we wish to update
		 * @return  HTTP Response representing state of the transaction..
		 */
	   @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCar(Car car){
	      int matchIdx = 0;
	      Optional<Car> match = cList.stream()
	          .filter(c->c.getCarId() == car.getCarId())
	          .findFirst();
	      if (match.isPresent()) {
	        matchIdx = cList.indexOf(match.get());
	        cList.set(matchIdx, car);
	        updateList(cList);
	        return Response.status(Response.Status.OK).build();
	      } 
	      
	      else 
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      
	    }
	   /**
		 *  Deletes the Car object with @param id 
		 * @param id The id of Car object we wish to delete
		 * @return  HTTP Response representing state of the transaction.
		 */
	    @DELETE
	    @Path("/{id}/delete")
	    public Response deleteCar(@PathParam("id") long id){
	      Predicate<Car> car = (c->c.getCarId() == id);
	      if (!cList.removeIf(car)) {
	       throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	      }
	      else {
	    	  Response temp = Response.status(Response.Status.OK).build();
	    	  return temp ;
	      }
	    }       
}
