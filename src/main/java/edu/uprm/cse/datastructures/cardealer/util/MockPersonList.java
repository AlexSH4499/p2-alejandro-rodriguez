package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.Person;

public class MockPersonList {
	private static final CopyOnWriteArrayList<Person> cList = new CopyOnWriteArrayList<Person>();
	
	  private MockPersonList(){}
	  
	  private static Person[] people= new Person[10];
		private static int[] age = new int[10];
		private static String[] firstname = new String[10];
		private static String[] lastname = new String[10];
		private static String[] phones = new String[10];
		private static char[] genders = {'f','m'};
		
		private static void firstName()
		{
			firstname[0] = "Jose";
			firstname[1] = "Juan";
			firstname[2] = "Maria";
			firstname[3] = "Luisa";
			firstname[4] = "Carmen";
			firstname[5] = "Xiomara";
			firstname[6] = "Zara";
			firstname[7] = "Enrique";
			firstname[8] = "Mario";
			firstname[9] = "Leo";
		}
		
		private static void lastName()
		{
			lastname[0] = "Rodriguez";
			lastname[1] = "Hernandez";
			lastname[2] = "Irizarry";
			lastname[3] = "Gonzalez";
			lastname[4] = "Rosario";
			lastname[5] = "Caraballo";
			lastname[6] = "Rivera";
			lastname[7] = "Torres";
			lastname[8] = "Natal";
			lastname[9] = "Cedeno";
		}

		private static void phones()
		{
			StringBuilder temp = new StringBuilder();
			int j = 0;
			Random rand = new Random();
			for(int k = 0 ; k < 10 ; k++)
			{
				for(int i = 0 ; i < 10; i++)
				{
					j = rand.nextInt(10);
					temp.append(j);
				}
				phones[k] = temp.toString();
				temp = new StringBuilder();
			}
		}
		
		private static void age()
		{
			int j = 0;
			Random rand = new Random();
			for(int k = 0 ; k < 10 ; k++)
			{

				age[k] = rand.nextInt(50);
			}
		}
		
		private static void people()
		{
			Random rand = new Random();
			firstName();
			lastName();
			phones();
			age();
			int a;
			String phone, ln, fn;
			char g;
			for(int i =0 ; i < people.length; i++)
			{
				 a = age[rand.nextInt(age.length)];
				 g = genders[rand.nextInt(genders.length)];
				 phone = phones[rand.nextInt(phones.length)];
				 ln = lastname[rand.nextInt(lastname.length)];
				 fn = firstname[rand.nextInt(firstname.length)];
				 people[i] = new Person(i, fn, ln,  a, g, phone);
			}
		}
	  
	  static {
//		  people();
//		  for(Person p: people)
//		  {
//			  cList.add(p);
//		  }
		  
	  }
	  public static CopyOnWriteArrayList<Person> getInstance(){
	    return cList;
	  }
	  
}
