package edu.uprm.cse.datastructures.cardealer.model;

/**
 * Person class used to model People for simulation purposes.
 * Parameters : unique ID, firstname, lastname, age, gender and phone.
 * @author Alejandro Rodriguez Natal
 *
 */
public class Person
{	
	private long personId; // internal id of the person
	private String firstName; // first name
	private String lastName; // lastname
	private Integer age; // age
	private char gender; // gender
	private String phone; // phone number
	
	
	
	//Constructors
	
	public Person()
	{
		//super();
	}
	
	public Person(long personId, String firstName, String lastName, Integer age, char gender, String phone)
	{
		this.personId = personId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.phone = phone;
	}
	
	//Setters
	/**
	 * Modifies Person object's ID.
	 * @return long representing ID.
	 */
	public void setPersonId(long pID)
	{
		this.personId = pID; 
	}
	
	/**
	 * Modifies Person object's first name.
	 * @return String representing first name.
	 */
	public void setFirstName(String n)
	{
		this.firstName = n; 
	}
	
	/**
	 * Modifies Person object's last name.
	 * @return String representing last name.
	 */
	public void setLastName(String n)
	{
		this.lastName = n; 
	}
	
	/**
	 * Modifies Person object's age.
	 * @return Integer representing age.
	 */
	public void setAge(Integer age)
	{
		this.age = age; 
	}
	
	/**
	 * Modifies Person object's gender.
	 * @return char representing gender.
	 */
	public void setGender(char g)
	{
		this.gender = g; 
	}
	
	/**
	 * Modifies Person object's phone number.
	 * @return String representing phone number.
	 */
	public void setPhone(String num)
	{
		this.phone = num; 
	}
	
	//Getters
	/**
	 * Retrieves Person object's unique ID.
	 * @return long representing Person's ID.
	 */
	public long getID()
	{
		return this.personId; 
	}
	
	/**
	 * Retrieves Person object's first name.
	 * @return String representing Person's first name.
	 */
	public String getFirstName()
	{
		return this.firstName ; 
	}

	/**
	 * Retrieves Person object's last name.
	 * @return String representing Person's last name.
	 */
	public String getLastName()
	{
		return this.lastName; 
	}
	
	/**
	 * Retrieves Person object's age.
	 * @return Integer representing Person's age.
	 */
	public Integer getAge()
	{
		return this.age; 
	}
	
	/**
	 * Retrieves Person object's gender.
	 * @return char representing Person's gender.
	 */
	public char getGender()
	{
		return this.gender ; 
	}
	
	/**
	 * Retrieves Person object's phone number.
	 * @return String representing Person's phone number.
	 */
	public String getPhone()
	{
		return this.phone; 
	}
	
	@Override
	public String toString()
	{
		return " [ ID:"+personId+", firstName:" +firstName +", lastName:"+ lastName+", Age:"+age
				+", Gender:"+gender+", PhoneNumber:"+phone+"]\n";
	}
}
