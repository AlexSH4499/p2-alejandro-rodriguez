package edu.uprm.cse.datastructures.cardealer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.model.AppointmentComparator;
import edu.uprm.cse.datastructures.cardealer.model.Person;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.LinkedPositionalList;
import edu.uprm.cse.datastructures.cardealer.util.MockAppointmentList;
import exceptions.JsonError;
import exceptions.NotFoundException;


/**
 * A Manager for Appointments
 * Keeps an Array of PositionalList for Appointments on a given week
 * The week starts on Monday(index 0) and goes all the way up to Friday(index 4).
 * @author Alejandro Rodriguez Natal
 *
 */
@Path("appointment")
public class AppointmentManager
{
	//private static final CopyOnWriteArrayList<Appointment> cList = MockAppointmentList.getInstance();   
	private  CircularSortedDoublyLinkedList<Appointment> sorting;
	//private LinkedPositionalList<Appointment> weekdays;
	//private ArrayList<LinkedPositionalList<Appointment>> weekdays;
	private static ArrayList<LinkedPositionalList<Appointment>> weekdays = MockAppointmentList.getInstance();
	private static HashMap<String , Integer> days =new HashMap<>();
	private static HashMap< Integer, String> daysInt =new HashMap<>();
		static 
		{
			if(days.isEmpty())
			{
			days.put("monday", 0 );
			days.put("tuesday", 1);
			days.put("wednesday", 2);
			days.put("thursday", 3);
			days.put("friday", 4);
			}
			
			if(daysInt.isEmpty())
			{
			daysInt.put( 0 ,"monday");
			daysInt.put( 1, "tuesday");
			daysInt.put( 2, "wednesday");
			daysInt.put( 3, "thursday");
			daysInt.put( 4, "friday");
			}
			if(weekdays.isEmpty())
				for(int i = 0 ; i < weekdays.size(); i++)
					weekdays.add( new LinkedPositionalList<Appointment>()); 
			
			
		}
		

		/**
		 * Keeps our inner list sorted
		 * @param cList the list we wish to sort.
		 */
	private void updateList(ArrayList<LinkedPositionalList<Appointment>> w)
	{
		sorting = new  CircularSortedDoublyLinkedList<Appointment>(new AppointmentComparator());

		for(LinkedPositionalList<Appointment> a: w)
		{
			for(Appointment b : a)
			{
				sorting.add(b);
			}
			
		}
		w.clear();
		
		for(int i = 0 ; i < 5; i++)
			w.add(new LinkedPositionalList<Appointment>());
		
		
		for(int i=0;i<sorting.size();i++)
		{
			int b  = days.get(sorting.get(i).getDay().toLowerCase());
			w.get(b).addLast(sorting.get(i));
			
		}
	
	}
	
	/**
	 * @return Retrieves all of our Appointments from @param cList
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAllAppointments()
	{
		ArrayList<Appointment> temp = new ArrayList<>();
		for(LinkedPositionalList<Appointment> list : weekdays)
		{
			for(Appointment a: list)
			{
				temp.add(a);
			}
		}
		//updateList(weekdays);
		return temp.toArray(new Appointment[0]);
	}
	
	
/**
 * Retrieves  Appointment with give @param id
 * @param id of Appointment to get from our list
 * @return Appointment with given id
 */
	  @GET
	  @Path("/{appointmentID}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Appointment getAppointment(@PathParam("appointmentID") long id)
	  {
		  boolean success = false;
		  Appointment temp = null;
		 for(LinkedPositionalList<Appointment> list : weekdays )
		 {
			 for(Appointment a : list)
			 {
				 if(a.getAppointmentId() == id)
				 {
					 temp = a;
					 success =true;
					 break;
				 }
			 }
			 if(success)
				 break;
		 } 
		 if(success)
			 return temp;
			 else {
	      throw new NotFoundException(new JsonError("Error", "Appointment " + id + " not found"));
		 }
	 }     
	  
		
		
	/**
	 * Retrieves  Appointment from given String @param day
	 * @param day String representing the day
	 * @return array of type Appointment representing Appointments on @param day
	 */
//	  @GET
//	  @Path("day/{day}")
//	  @Produces(MediaType.APPLICATION_JSON)
//	  public Appointment[] getAppointmentByDay(@PathParam("day") String day)
//	  {
//		  ArrayList<Appointment> dummyList = new ArrayList<>(); 
//		  if(days.containsKey(day.toLowerCase()))
//		  {
//			  int  d = days.get(day.toLowerCase());
//			  for(Appointment a: weekdays.get(d))
//			  {
//				  dummyList.add(a);
//			  }
//			  
//		  }
//	
//	    if (!dummyList.isEmpty())
//	      return dummyList.toArray(new Appointment[0]);
//	     
//	    else 
//	    	 throw new NotFoundException(new JsonError("Error", "Appointment of day " + day + " not found"));
//	    
//	 }  
	  
	  
	  /**
		 * Retrieves  Appointment from given int  @param day.
		 * @param day int representing the day.
		 * @return array of type Appointment representing Appointments on @param day
		 */
		  @GET
		  @Path("day/{day}")
		  @Produces(MediaType.APPLICATION_JSON)
		  public Appointment[] getAppointmentByDay(@PathParam("day") int day)
		  {
			  ArrayList<Appointment> dummyList = new ArrayList<>(); 
			  if(daysInt.containsKey(day))
			  {
				  
				  for(Appointment a: weekdays.get(day))
				  {
					  dummyList.add(a);
				  }
				  
			  }
		
		    if (!dummyList.isEmpty())
		      return dummyList.toArray(new Appointment[0]);
		     
		    else 
		    	 throw new NotFoundException(new JsonError("Error", "Appointment of day " + day + " not found"));
		    
		 }  
	    
	  /**
	   * Add Appointment to our inner list with String @param day.
	   * @param p Appointment to add
	   * @return HTTP Response
	   */
//	   @POST
//	    @Path("/add/{day}")
//	    @Produces(MediaType.APPLICATION_JSON)
//	    public Response addAppointment(@PathParam("day") String day , Appointment p)
//	   {
//		   //String day = day.toLowerCase();
//		   day = day.toLowerCase();
//		   if(days.containsKey(day))
//		   {
//			   int d = days.get(day);
//			   p.setDay(day);
//			   boolean inList = false;
//			   for(Appointment a: weekdays.get(d))
//			   {
//				   if(a.getAppointmentId() == p.getAppointmentId())
//				   {
//					   inList = true;
//					   break;
//				   }
//			   }
//			   
//			   if(!inList)
//			   {
//				   
//				   weekdays.get(d).addLast(p);
//				   return Response.status(201).build();
//			   }
//		      
//		     
//		   }
//		  
//		   return Response.status(201).build();
//	    }  
//	   
	   
		  /**
		   * Adds Appointment to our inner list with int @param day.
		   * @param p Appointment to add
		   * @return HTTP Response
		   */
		   @POST
		    @Path("/add/{day}")
		    @Produces(MediaType.APPLICATION_JSON)
		    public Response addAppointment(@PathParam("day") int day , Appointment p)
		   {
			   //String day = day.toLowerCase();
			   
			   if(daysInt.containsKey(day))
			   {
				   String d = daysInt.get(day);
				   p.setDay(d);
				   boolean inList = false;
				   for(Appointment a: weekdays.get(day))
				   {
					   if(a.getAppointmentId() == p.getAppointmentId())
					   {
						   inList = true;
						   break;
					   }
				   }
				   
				   if(!inList)
				   {
					   
					   weekdays.get(day).addLast(p);
					   return Response.status(201).build();
				   }
			      
			     
			   }
			  
			   return Response.status(201).build();
		    }  
	   
	   /**
	    * Retrieves index representing internal position of @param a if it exists.
	    * @param a Appointment Object whose index inside our list we want to know
	    * @return int representing index, if negative then not found in list.
	    */
	   private int indexOf(Appointment a)
	   {
		  int index = -1;
		   
		   for(int i = 0 ; i < weekdays.size(); i ++)
		   {
			   for(Appointment ap : weekdays.get(i))
			   {
				   if(ap.equals(a))
				   {
					   index = i;
					   break;
				   }
			   }
		   }
		   
		   return index;
	   }
	   
	   /**
	    * Determines if @param a is in list.
	    * @param a Appointment Object whose existence we want to know
	    * @return boolean representing existence in list.
	    */
	   private boolean inList(Appointment a)
	   {
		   boolean inList = false;
		   
		   for(LinkedPositionalList<Appointment> list : weekdays)
		   {
			   for(Appointment ap: list)
			   {
				   if(ap.equals(a))
				   {
					   inList = true;
					   break;
				   }
			   }
		   }
		   
		   return inList;
	   }
		  /**
		   * Update Appointment @param p of our inner list.
		   * @param p Appointment to update
		   * @return HTTP Response
		   */
	   @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateAppointment(Appointment p)
	   {
		   boolean success = false;
	      if (inList(p)) 
	      {
	    	  
	    	int day = indexOf(p);
	    	   		
	    	for(Appointment a: weekdays.get(day))
	    	{
	    		if(a.getAppointmentId() == p.getAppointmentId())
	    		{
	    			success = true;
	   				a = p;
	   				break;
	   			}
	    	}
	    			
	    		
	      } 
	      if(success)
	    	  return Response.status(Response.Status.OK).build();
	      else 
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      
	    }
	   
		  /**
		   * Delete Appointment with @param id from our inner list.
		   * @param id Appointment to delete
		   * @return HTTP Response
		   */
	    @DELETE
	    @Path("/{id}/delete")
	    public Response deleteAppointment(@PathParam("id") long id)
	    {
	    	 boolean success = false;
		    	Appointment temp = null;
	    	 for(LinkedPositionalList<Appointment> day: weekdays)
	    	 {
		    		Iterator<Appointment> iter = day.iterator();
		    		while(iter.hasNext())
		    		{
		    			temp = iter.next();
		    			if(temp.getAppointmentId() == id)
		    			{
		    				success = true;
		    				iter.remove();
		    				break;
		    			}
		    		}
		    		if(success)
		    			break;
	    	 }	
		
		      if(success)
		    	  return Response.status(Response.Status.OK).build();
		      else 
		        return Response.status(Response.Status.NOT_FOUND).build();      
		      
	    } 
}
