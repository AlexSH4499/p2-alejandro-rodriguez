package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;

public class MockAppointmentList {
	private static final ArrayList<LinkedPositionalList<Appointment>> cList = new ArrayList<>();
	private static int SIZE = 10;
	private static Appointment[] appointments = new Appointment[SIZE];
	private static String[] jobs = {"Oil Change","Check Engine","Motor Failure", 
							"Change Tires", "Change Filter", "Alignment", "Break Change"};
	private static String[] weekdays = {"monday","tuesday","wednesday","thursday","friday"};
	private static Random rand = new Random();

	private static HashMap<String , Integer> days =new HashMap<>();
	
	
	
  private MockAppointmentList(){}
	  
	  
	  static {
//		for(int i =  0;  i < appointments.length; i++)
//				{
//					 appointments[i] = new Appointment(i , i , jobs[rand.nextInt(jobs.length)], weekdays[rand.nextInt(weekdays.length)]);
//				
//				}
		if(days.isEmpty())
		{
		days.put("monday", 0 );
		days.put("tuesday", 1);
		days.put("wednesday", 2);
		days.put("thursday", 3);
		days.put("friday", 4);
		}
		for(int i = 0 ; i < 5; i++)
		{
			cList.add(new LinkedPositionalList<Appointment>());
		}
	  }
	  public static ArrayList<LinkedPositionalList<Appointment>> getInstance(){
//		  for(Appointment a : appointments)
//		  {
//			  cList.get(days.get(a.getDay().toLowerCase())).addLast(a);
//		  }
		  
	    return cList;
	  }
	  
}
