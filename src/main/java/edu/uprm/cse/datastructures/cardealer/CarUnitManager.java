package edu.uprm.cse.datastructures.cardealer;

import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.CarUnitComparator;
import edu.uprm.cse.datastructures.cardealer.model.Person;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.MockCarUnitList;
import exceptions.JsonError;
import exceptions.NotFoundException;

/**
 * Manager for CarUnit objects that keeps track and sorts the units by VIN.
 * @author Alejandro Rodriguez Natal
 *
 */
@Path("carunit")
public class CarUnitManager
{

	private static CopyOnWriteArrayList<CarUnit> cList = MockCarUnitList.getInstance();   
	private  CircularSortedDoublyLinkedList<CarUnit> sorting;
	
	public static CopyOnWriteArrayList<CarUnit> getList()
	{
		return cList;
	}
	
	/**
	 * Verifies that @param c has a unique VIN in our internal list.
	 * @param c CarUnit whose VIn we want to verify.
	 * @return boolean representing if our list already contains @param c 's VIN.
	 */
	private boolean existingVIN(CarUnit c)
	{
		boolean isInList = false;
		
		for(CarUnit car: cList)
		{
			if(car.getVIN().equals(c.getVIN()))
			{
				isInList = true;
				//System.out.println(" [!] ILLEGAL ARGUMENT: VIN IS ALREADY IN LIST");
				break;
			}
		}
		
		return isInList;
	}
	
	/**
	 * Sorts our internal list by VIN
	 * @param cList the list we will be sorting.
	 */
	private void updateList(CopyOnWriteArrayList<CarUnit> cList)
	{
		sorting = new  CircularSortedDoublyLinkedList<CarUnit>(new CarUnitComparator());

		for(CarUnit a: cList) 
		{
				sorting.add(a);
		}
		cList.clear();
		
		for(int i=0;i<sorting.size();i++) {
			cList.add((CarUnit) sorting.get(i));
			
		}
	
	}
	
	/**
	 * Returns an array of type CarUnit representing the internal list. 
	 * @return array of type CarUnit.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public CarUnit[] getAllCarUnits()
	{
		updateList(cList);
		return cList.toArray(new CarUnit[0]);
	}
	
	/**
	 *  Retrieves the CarUnit object with id matching @param id
	 * @param id The id of the CarUnit object we wish to retrieve
	 * @return The CarUnit object with provided id.
	 */
	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public CarUnit getCarUnit(@PathParam("id") long id){
	    Optional<CarUnit> match
	        = cList.stream()
	        .filter(c->c.getCarUnitId() == id)
	        .findFirst();
	    if (match.isPresent()) {
	    	updateList(cList);
	      return match.get();
	    } else {
	      throw new NotFoundException(new JsonError("Error", "CarUnit " + id + " not found"));
	    }
	 }     
	  
		/**
		 *  Retrieves the CarUnit object with vin matching @param vin
		 * @param vin The vin of the CarUnit object we wish to retrieve
		 * @return The CarUnit object with provided VIN.
		 */
	  @GET
	  @Path("/VIN/{VIN}")//TODO changed this from /{VIN}
	  @Produces(MediaType.APPLICATION_JSON)
	  public CarUnit[] getAllCarUnitsByVIN(@PathParam("VIN") String vin)
	  {
		  CopyOnWriteArrayList<CarUnit> dummyList = new CopyOnWriteArrayList(); 
		  Iterator<CarUnit> iter = cList.iterator();
		  CarUnit ptr = iter.next();
		  if(ptr.getVIN().equalsIgnoreCase( vin))
		  {
			  System.out.println(ptr);
			  dummyList.add(ptr);
		  }
		  while(iter.hasNext())
		  {
			  if(ptr.getVIN().equalsIgnoreCase(vin))
			  {
				  System.out.println(ptr);
				  dummyList.add(ptr);
			  }
			  ptr = iter.next();
		  }
	  
	    if (!dummyList.isEmpty())
	    {
	    	updateList(dummyList);
	      return dummyList.toArray(new CarUnit[0]);
	    } else {
	      throw new NotFoundException(new JsonError("Error", "CarUnit of VIN " + vin + " not found"));
	    }
	 }     
	    
	  //TODO FIX IT HOW THE CHAT SAID, BY UNIQUE carID I THINK IT WAs
	  /**
	   * Adds @param p to our inner list
	   * @param p CarUnit we wish to add to list
	   * @return HTTP Response representing state of the transaction.
	   */
	   @POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addCarUnit(CarUnit p)
	   {
		   if(!existingVIN(p))
		   {	cList.add(p);
		   		updateList(cList);
		   	 //return Response.status(201).build();
		   }
	      return Response.status(201).build();
	    }  
	   
		/**
		 *  Updates the Car object with carUnitId matching Path id
		 * @param p The CarUnit object we wish to update
		 * @return  HTTP Response representing state of the transaction..
		 */
	   @PUT
	    @Path("/{carUnitId}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCarUnit(CarUnit p){
	      int matchIdx = 0;
	      Optional<CarUnit> match = cList.stream()
	          .filter(c->c.getCarUnitId() == p.getCarUnitId())
	          .findFirst();
	      if (match.isPresent()) {
	        matchIdx = cList.indexOf(match.get());
	        cList.set(matchIdx, p);
	        updateList(cList);
	        return Response.status(Response.Status.OK).build();
	      } else {
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      }
	    }
	   
	   /**
	 		 *  Deletes the CarUnit object with @param id 
	 		 * @param id The id of CarUnit object we wish to delete
	 		 * @return  HTTP Response representing state of the transaction.
	 		 */
	    @DELETE
	    @Path("/{carUnitId}/delete")
	    public Response deleteCarUnit(@PathParam("id") long id){
	      Predicate<CarUnit> p = (c->c.getCarUnitId() == id);
	      if (!cList.removeIf(p)) {
	       throw new NotFoundException(new JsonError("Error", "CarUnit " + id + " not found"));
	      }
	      else {
	    	  Response temp = Response.status(Response.Status.OK).build();
	    	  return temp ;
	      }
	    } 
}
