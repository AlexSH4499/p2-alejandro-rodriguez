package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.util.AbstractComparator;

/**
 * Car comparator used to order Appointments in a list.
 * The criteria for Appointment sorting is based on:  brand, model, option and year.(In that order)
 * @author Alejandro Rodriguez Natal
 *
 */

public class CarComparator extends AbstractComparator<Car>
{
	//Assume Objects provided aren't null

	@Override
	public int compare(Car o1, Car o2) 
	{
		if(compareBrands(o1,o2)!= 0)
			return compareBrands(o1,o2);
		
		if(compareModels(o1,o2)!= 0)
			return compareModels(o1,o2);
		
		if(compareModelOption(o1,o2)!= 0)
			return compareModelOption(o1,o2);
		
		if(comparePrices(o1,o2) != 0)
			return comparePrices(o1,o2);
			
		return 0;
			
	}
	
	/*Helper methods for compare*/
	/**Compare Model Options of @param car1 and @param car 2 
	 */
	private int compareModelOption(Car car1, Car car2)
	{
		if(car1 == null |car2 == null )
			return 0;
		String car1ModelOption = car1.getCarModelOption();
		String car2ModelOption = car2.getCarModelOption();
		
		if(car1ModelOption==car2ModelOption)
			return 0;
		return car1ModelOption.compareToIgnoreCase(car2ModelOption);
	}
	
	/**Compare Models of @param car1 and @param car 2 
	 */
	private int compareModels(Car car1, Car car2)
	{
		if(car1 == null |car2 == null )
			return 0;
		String car1Model = car1.getCarModel();
		String car2Model = car2.getCarModel();
		
		if(car1Model==car2Model)
			return 0;
		
		return car1Model.compareToIgnoreCase(car2Model);
	}
	
	/**Compare Brands of @param car1 and @param car 2 
	 */
	private int compareBrands(Car car1, Car car2) 
	{
		if(car1 == null |car2 == null )
			return 0;
		
		String car1Brand = car1.getCarBrand();
		String car2Brand = car2.getCarBrand();
		
		if(car1Brand==car2Brand)
			return 0;

		
		return car1Brand.compareToIgnoreCase(car2Brand);
	}

	/**Compares Prices of @param car1 and @param car 2 
	 */
	private int comparePrices(Car car1, Car car2)
	{
		if (car1.getCarYear()>car2.getCarYear())  return 1 ;
		if (car1.getCarYear()<car2.getCarYear())  return -1 ;
		
		return 0;
	}
}
