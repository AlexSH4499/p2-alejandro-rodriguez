package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.AbstractComparator;
/**
 * Person comparator used to order people in a list.
 * The criteria for Person sorting is based on:  last name and first name.(In that order)
 * @author Alejandro Rodriguez Natal
 *
 */
public class PersonComparator extends AbstractComparator<Person> 
{
	private int compareID(Person p1, Person p2)
	{
		int i = 0;
		
		if(p1.getID() > p2.getID())
			i = 1;
		if(p1.getID() <p2.getID())
			i =-1;
		
		return i;
	}
	
	private int compareGender(Person p1, Person p2)
	{
		int i = 0;
		
		
		if(Character.toLowerCase(p1.getGender()) > Character.toLowerCase(p2.getGender()))
			i = 1;
		if(Character.toLowerCase(p1.getGender()) < Character.toLowerCase(p2.getGender()))
			i =-1;
		
		return i;
	}
	
	//TODO incomplete comparator for Person objects!
	@Override
	public int compare(Person p1, Person p2)
	{
		if(p1.getLastName().compareTo(p2.getLastName())!= 0)
			return p1.getLastName().compareTo(p2.getLastName());
		if(p1.getFirstName().compareTo(p2.getFirstName())!= 0)
			return p1.getFirstName().compareTo(p2.getFirstName());
		
		return 0;
	}
}
