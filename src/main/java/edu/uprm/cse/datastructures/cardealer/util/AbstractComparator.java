package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

/**
 * Comparator object for comparing two objects of type <Object> 
 * @author Alejandro Rodriguez Natal
 *
 * @param <Object> what we want to compare
 */
public class AbstractComparator<Object> implements Comparator<Object> {

	/**
	 * Compares @param arg0 with @param arg1 resulting in an int in range  [-1,1]
	 * @param arg0 First <Object> which we want to compare
	 * @param arg1 Second <Object> which we want to compare
	 * @return int in range  [-1,1] where -1 means less than, 0 equal and 1 greater than.
	 */
	public int compare(Object arg0, Object arg1) {
		if(arg0.equals(arg1))
			return 0;
		if(!arg0.equals(arg1))
			return 1;
		
		return -1;
	}

}
