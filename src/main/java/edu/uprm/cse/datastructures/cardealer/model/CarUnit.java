package edu.uprm.cse.datastructures.cardealer.model;

/**
 * CarUnit class to represent cars being handled at a shop.
 * Parameters: car ID , car unit ID, VIN, color, carPlate, personID;
 * @author Alejandro Rodriguez Natal
 *
 */
public class CarUnit
{
	private long carUnitId; // internal id of the unit
	private long carId; // id of the car object that represents the general for the car. This 
                                           //Car from project 1.
	private String vin; // vehicle identification number
	private String color; // car color
	private String carPlate; // car plate (null until sold)
	private long personId; // id of the person who purchased the car. (null until //purchased)	
	
	public CarUnit()
	{
		
	}
	
	public CarUnit(long CUID, long CID, String VIN,String color, String carplate, long personID )
	{
		this.carUnitId = CUID;
		this.carId = CID;
		this.vin = VIN;
		this.color = color;
		this.carPlate = carplate;
		this.personId = personID;
	}
	
	//Makes the carplate empty if not bought yet
	public CarUnit(long CUID, long CID, String VIN,String color,  long personID)
	{
		this(CUID,CID,VIN,color,"",personID);
	}
	
	/**
	 * Retrieves CarUnit object's unique ID.
	 * @return long representing ID
	 */
	public long getCarUnitId() {
		return carUnitId;
	}
	
	/**
	 * Retrieves CarUnit object's  Car ID.
	 * @return long representing Car ID
	 */
	public long getCarId() {
		return carId;
	}
	
	
	/**
	 * Retrieves CarUnit object's VIN.
	 * @return String representing VIN
	 */
	public String getVIN() {
		return vin;
	}
	
	/**
	 * Retrieves CarUnit object's unique ID.
	 * @return String representing Car of CarUnit's  color.
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * Retrieves CarUnit object's car plate.
	 * @return String representing CarUnit's  car plate.
	 */
	public String getCarPlate() {
		return carPlate;
	}
	
	/**
	 * Retrieves CarUnit object's person ID.
	 * @return long representing CarUnit's  person ID.
	 */
	public long getPersonId() {
		return personId;
	}
	
	/**
	 * Modifies CarUnit object's carUnitID.
	 * @param carUnitId long representing CarUnit's  carUnitID.
	 */
	public void setCarUnitId(long carUnitId) {
		this.carUnitId = carUnitId;
	}
	
	/**
	 * Modifies CarUnit object's car ID.
	 * @param carId long representing CarUnit's  car ID.
	 */
	public void setCarId(long carId) {
		this.carId = carId;
	}
	
	/**
	 * Modifies CarUnit object's VIN.
	 * @param vIN String representing CarUnit's  VIN.
	 */
	public void setVIN(String vIN) {
		vin = vIN;
	}
	
	/**
	 * Modifies CarUnit object's color.
	 * @param color String representing CarUnit's  color.
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Modifies CarUnit object's carPlate.
	 * @param carPlate String representing CarUnit's  carPlate.
	 */
	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}
	
	/**
	 * Modifies CarUnit object's person ID.
	 * @param personID long representing CarUnit's  person ID.
	 */
	public void setPersonId(long personId) {
		this.personId = personId;
	}
}
