package edu.uprm.cse.datastructures.cardealer.util;

import java.util.concurrent.CopyOnWriteArrayList;

import edu.uprm.cse.datastructures.cardealer.model.CarUnit;

public class MockCarUnitList {
	private static final CopyOnWriteArrayList<CarUnit> cList = new CopyOnWriteArrayList<CarUnit>();
	
	  private MockCarUnitList(){}
	  
	  
	  static {
		  
	  }
	  public static CopyOnWriteArrayList<CarUnit> getInstance(){
	    return cList;
	  }
	  
}
