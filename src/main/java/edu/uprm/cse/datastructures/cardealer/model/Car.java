package edu.uprm.cse.datastructures.cardealer.model;
// Car class pojo

/**
 * Car representation class that contains parameters: unique ID,  brand, model, model option, year and price.
 * 
 * @author Alejandro Rodriguez Natal
 *
 */
public class Car
{
	private long carId;
	private String carBrand;
	private String carModel;
	private String carModelOption;
	private int carYear;
	private double carPrice;
	
	//TODO change this back to super();
	public Car() {
		this(-1, "", "","",1000,-2000000.50);
	}
	public Car(long carId, String carBrand, String carModel, String carModelOption, double carPrice) {
		this(carId, carBrand, carModel,carModelOption,2000,carPrice);
	}
	
	public Car(long carId, String carBrand, String carModel, String carModelOption,int year, double carPrice) {
		super();
		this.carId = carId;
		this.carBrand = carBrand;
		this.carModel = carModel;
		this.carModelOption = carModelOption;
		this.setCarYear(year);
		this.carPrice = carPrice;
	}
	
	/*Getters*/
	
	/**
	 * Retrieves unique ID of Car object
	 * @return long representing ID of Car object
	 */
	public long getCarId() {
		return carId;
	}
	
	/**
	 * Retrieves brand of Car object
	 * @return String representing brand of Car object
	 */
	public String getCarBrand() {
		return carBrand;
	}
	
	/**
	 * Retrieves model of Car object
	 * @return String representing model of Car object
	 */
	public String getCarModel() {
		return carModel;
	}
	

	/**
	 * Retrieves model option of Car object
	 * @return String representing model option of Car object
	 */
	public String getCarModelOption() {
		return carModelOption;
	}
	
	/**
	 * Retrieves price of Car object
	 * @return double representing price of Car object
	 */
	public double getCarPrice() {
		return carPrice;
	}
	
	/**
	 * Retrieves year of Car object
	 * @return int representing year of Car object
	 */
	public int getCarYear() {
		return carYear;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carBrand == null) ? 0 : carBrand.hashCode());
		result = prime * result + (int) (carId ^ (carId >>> 32));
		result = prime * result + ((carModel == null) ? 0 : carModel.hashCode());
		result = prime * result + ((carModelOption == null) ? 0 : carModelOption.hashCode());
		long temp;
		temp = Double.doubleToLongBits(carPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + carYear;
		return result;
	}

	/*Setters*/
	/**
	 * Assigns @param id as this Car object's new unique ID.
	 * @param id int representing Car object's ID.
	 */
	public void setCarID(int id) {
		this.carId = id;
	}
	
	/**
	 * Assigns @param carYear as this Car object's new year.
	 * @param year int representing the Car object's year of manufacture.
	 */
	public void setCarYear(int carYear) {
		this.carYear = carYear;
	}
	
	/**
	 * Assigns @param price as this Car object's new price.
	 * @param price double representing the Car object's price.
	 */
	public void setCarPrice(double price) {
		this.carPrice = price;
	}
	
	/**
	 * Assigns @param brand as this Car object's new brand.
	 * @param brand String representing the Car object's brand.
	 */
	public void setCarBrand(String brand) {
		this.carBrand = brand;
	}
	
	/**
	 * Assigns @param model as this Car object's new model.
	 * @param model String representing the Car object's model.
	 */
	public void setCarModel(String model) {
		this.carModel = model;
	}
	
	/**
	 * Assigns @param option as this Car object's new model option.
	 * @param option String representing the Car object's model option.
	 */
	public void setCarModelOption(String option) {
		this.carModelOption = option;
	}
	
	/**
	 * Verifies that this object and @param obj are the same object
	 * returns true if they are.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (carBrand == null) {
			if (other.carBrand != null)
				return false;
		} else if (!carBrand.equals(other.carBrand))
			return false;
		if (carId != other.carId)
			return false;
		if (carModel == null) {
			if (other.carModel != null)
				return false;
		} else if (!carModel.equals(other.carModel))
			return false;
		if (carModelOption == null) {
			if (other.carModelOption != null)
				return false;
		} else if (!carModelOption.equals(other.carModelOption))
			return false;
		if (Double.doubleToLongBits(carPrice) != Double.doubleToLongBits(other.carPrice))
			return false;
		if (carYear != other.carYear)
			return false;
		return true;
	}
	
	/**
	 * String representation of this instance of Car object.
	 */
	@Override
	public String toString() {
		return "Car [carId=" + carId + ", carBrand=" + carBrand + ", carModel=" + carModel + ", carModelOption="
				+ carModelOption +", carYear="+carYear +", carPrice=" + carPrice + "]";
	}
	
}
