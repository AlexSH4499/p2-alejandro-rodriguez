package edu.uprm.cse.datastructures.cardealer.model;

/**
 * Class for defining Appointments with day, bill, job, carUnit ID and internal appointment ID.
 * @author Alejandro Rodriguez Natal
 *
 */
public class Appointment
{

	private long appointmentId; // internal id of the appointment
	private long carUnitId; // id of the car to be serviced
	private String job; // description of the job to be done (i.e.: �oil change�)
	private double bill; // cost of the service (initially 0).
	private String day;
	
	/* Constructors */
	
	public Appointment()
	{
		//this(appID, carUID, job, 0.0, day);
	}
	
	public Appointment(long appID, long carUID, String job, String day)
	{
		this(appID, carUID, job, 0.0, day);
	}
	
	public Appointment(long appID, long carUID, String job, double bill)
	{
		this.setAppointmentId(appID);
		this.setCarUnitId(carUID);
		this.setJob(job);
		this.setBill(bill);
		//this(appID, carUID,job, bill,"");
	}
	
	public Appointment(long appID, long carUID, String job, double bill, String day)
	{
		this.setAppointmentId(appID);
		this.setCarUnitId(carUID);
		this.setJob(job);
		this.setBill(bill);
		this.setDay(day);
	}

	
	/*Methods*/
	

	//Setters
	/**
	 * Assigns @param carUnitId as this object's new car unit ID.
	 * @param carUnitId long representing the new car unit ID.
	 */
	public void setCarUnitId(long carUnitId) {
		this.carUnitId = carUnitId;
	}
	
	/**
	 * Assigns @param appointmentId as this object's new appointment ID.
	 * @param appointmentId long representing the new appointmentId.
	 */
	public void setAppointmentId(long appointmentId) {
		this.appointmentId = appointmentId;
	}

	/**
	 * Assigns @param bill as this object's new bill.
	 * @param bill double representing the new bill.
	 */
	public void setBill(double bill) {
		this.bill = bill;
	}

	/**
	 * Assigns @param job as this object's new job.
	 * @param job String representing the new job.
	 */
	public void setJob(String job) {
		this.job = job;
	}
	

	/**
	 * Assigns @param day as this object's new day.
	 * @param day String representing the new day.
	 */
	public void setDay(String day) {
		this.day = day;
	}
	
	//Getters
	
	/**
	 * Retrieves internal Appointment ID
	 * @return long representing internal Appointment ID parameter
	 */
	public long getAppointmentId() {
		return appointmentId;
	}


	/**
	 * Retrieves internal job
	 * @return String representing internal job parameter
	 */
	public String getJob() {
		return job;
	}

	

	/**
	 * Retrieves internal bill
	 * @return double representing internal bill parameter
	 */
	public double getBill() {
		return bill;
	}


	/**
	 * Retrieves internal day
	 * @return String representing internal day parameter
	 */
	public String getDay() {
		return day;
	}
	
	/**
	 * Retrieves internal carunit ID
	 * @return long representing carunit ID parameter
	 */
	public long getCarUnitId() {
		return carUnitId;
	}

	
}
