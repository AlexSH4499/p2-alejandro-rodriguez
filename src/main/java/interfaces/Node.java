package interfaces;

/**
 * Nodes to be used in various Data Structures. At minimum should have a next and an element of type @param <E>
 * 
 * @author Alejandro Rodriguez Natal
 *
 * @param <E> object of which the Node will be made of.
 */
public interface Node<E> {
	E getElement();
	Node<E> getNext();
}
