package testers;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.model.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class PersonTester {

	private CircularSortedDoublyLinkedList<Person> list = new CircularSortedDoublyLinkedList<Person>(new PersonComparator());
	private Person[] people= new Person[10];
	private int[] age = new int[10];
	private String[] firstname = new String[10];
	private String[] lastname = new String[10];
	private String[] phones = new String[10];
	private char[] genders = {'f','m'};
	
	private void firstName()
	{
		firstname[0] = "Jose";
		firstname[1] = "Juan";
		firstname[2] = "Maria";
		firstname[3] = "Luisa";
		firstname[4] = "Carmen";
		firstname[5] = "Xiomara";
		firstname[6] = "Zara";
		firstname[7] = "Enrique";
		firstname[8] = "Mario";
		firstname[9] = "Leo";
	}
	
	private void lastName()
	{
		lastname[0] = "Rodriguez";
		lastname[1] = "Hernandez";
		lastname[2] = "Irizarry";
		lastname[3] = "Gonzalez";
		lastname[4] = "Rosario";
		lastname[5] = "Caraballo";
		lastname[6] = "Rivera";
		lastname[7] = "Torres";
		lastname[8] = "Natal";
		lastname[9] = "Cedeno";
	}

	private void phones()
	{
		StringBuilder temp = new StringBuilder();
		int j = 0;
		Random rand = new Random();
		for(int k = 0 ; k < 10 ; k++)
		{
			for(int i = 0 ; i < 10; i++)
			{
				j = rand.nextInt(10);
				temp.append(j);
			}
			phones[k] = temp.toString();
			temp = new StringBuilder();
		}
	}
	
	private void age()
	{
		int j = 0;
		Random rand = new Random();
		for(int k = 0 ; k < 10 ; k++)
		{

			age[k] = rand.nextInt(50);
		}
	}
	
	private void people()
	{
		Random rand = new Random();
		firstName();
		lastName();
		phones();
		age();
		int a;
		String phone, ln, fn;
		char g;
		for(int i =0 ; i < people.length; i++)
		{
			 a = age[rand.nextInt(age.length)];
			 g = genders[rand.nextInt(genders.length)];
			 phone = phones[rand.nextInt(phones.length)];
			 ln = lastname[rand.nextInt(lastname.length)];
			 fn = firstname[rand.nextInt(firstname.length)];
			 people[i] = new Person(i, fn, ln,  a, g, phone);
		}
	}
	
	@Test
	public void test() 
	{
		people();//Generate people
		
		System.out.println("Array of people:");
		for(Person p : people)
		{
			System.out.println(p);
		}
		System.out.println();
		
		
		for(Person p : people)
		{
			list.add(p);
		}
		
		list.printList();
	}

}
